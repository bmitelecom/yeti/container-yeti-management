# yeti-management in a container
#### Containerized version of yeti-management daemon (part of the "Yeti switch" project)
We are aimed at contenerization of yeti-management in order to provide an opportunity to run it
in Docker and other container services.

##Version notification
Please be informed that versions of our containers might be different from versions of included services 
(e.g. the version of yeti-manamegent is 1.0.20 but the version of container is 1.0.1)
